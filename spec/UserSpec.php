<?php
use App\Post;
use Sofa\LaravelKahlan\Env;

describe('User', function () {

   beforeEach(function() {
    $this->laravel->artisan('migrate');
   });

   using(['database transactions'], function(){

       given('user', function () {
        return  factory(\App\User::class)->create();
    });

    it('should delete post when a user is deleted', function () {
        $this->user->posts()->create(['name' => "victor"]);
        $this->user->posts()->create(['name' => "test"]);
        $this->user->posts()->create(['name' => "1"]);
        expect(Post::count())->toBe(3);
        $this->user->delete();
        expect(Post::count())->toBe(0);
        

    });

   });

    

});